class nc_nginx {

  file { "/opt/hybris/nginx":
    ensure => directory,
  }

  file { "/opt/hybris/nginx.tgz": 
    source => "puppet:///modules/nc_nginx/nginx.tgz",
  }

  exec { "unzip-nginx" :
    command     => '/bin/tar -C /opt/hybris/nginx/ -xf /opt/mss/nginx.tgz',
    creates     => '/opt/hybris/nginx/nginx',
    logoutput   => true,
  }

  # proxy: -x 10.41.5.10:3128
#  exec { "download-nginx.gz":
#    command => "/usr/bin/curl -s  http://ncrepo.netconomy.net/nginx/nginx-8u45-linux-x64.gz -o /opt/nginx/nginx-8u45-linux-x64.gz",
#    creates => "/opt/hybris/nginx-8u45-linux-x64.gz",
#    notify  => Exec['unzip-fredhopper.zip'],
#  }

}
