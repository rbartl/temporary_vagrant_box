class nc_jdk {

  file { "/opt/hybris/jdk":
    ensure => directory,
  }

  file { "/opt/hybris/jdk-8u45-linux-x64.gz": 
    source => "puppet:///modules/nc_jdk/jdk-8u45-linux-x64.gz",
  }

  exec { "unzip-fredhopper.zip" :
    command     => '/bin/tar -C /opt/hybris/jdk/ --strip 1 -xvf /opt/mss/jdk-8u45-linux-x64.gz',
    creates     => '/opt/hybris/jdk/LICENSE',
  }

  file { "/root/.bashrc":
    ensure => present,
  }->
  file_line { "Append a line to /root/.bashrc":
    path => "/root/.bashrc",  
    line => "export PATH=\$PATH:/opt/hybris/jdk/bin/",
    match   => "/opt/hybris/jdk/bin",
  }

  # proxy: -x 10.41.5.10:3128
#  exec { "download-jdk.gz":
#    command => "/usr/bin/curl -s  http://ncrepo.netconomy.net/jdk/jdk-8u45-linux-x64.gz -o /opt/jdk/jdk-8u45-linux-x64.gz",
#    creates => "/opt/hybris/jdk-8u45-linux-x64.gz",
#    notify  => Exec['unzip-fredhopper.zip'],
#  }

}
