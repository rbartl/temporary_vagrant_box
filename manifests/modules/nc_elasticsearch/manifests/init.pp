class nc_elasticsearch {

  file { "/opt/hybris/elasticsearch":
    ensure => directory,
  }

  file { "/opt/hybris/elasticsearch-1.5.2.tar.gz": 
    source => "puppet:///modules/nc_elasticsearch/elasticsearch-1.5.2.tar.gz",
  }

  exec { "unzip-elasticsearch" :
    command     => '/bin/tar --strip 1 -C /opt/hybris/elasticsearch/ -xf /opt/mss/elasticsearch-1.5.2.tar.gz',
#    creates     => "/opt/hybris/elasticsearch/bin/elasticsearch",
  }

  # proxy: -x 10.41.5.10:3128
#  exec { "download-elasticsearch.gz":
#    command => "/usr/bin/curl -s  http://ncrepo.netconomy.net/elasticsearch/elasticsearch-8u45-linux-x64.gz -o /opt/elasticsearch/elasticsearch-8u45-linux-x64.gz",
#    creates => "/opt/hybris/elasticsearch-8u45-linux-x64.gz",
#    notify  => Exec['unzip-fredhopper.zip'],
#  }

}
