class nc_haproxy {

  file { "/opt/hybris/haproxy":
    ensure => directory,
  }

  file { "/opt/hybris/haproxy/haproxy": 
    source => "puppet:///modules/nc_haproxy/haproxy",
    mode   => '0755',
  }

  # proxy: -x 10.41.5.10:3128
#  exec { "download-haproxy.gz":
#    command => "/usr/bin/curl -s  http://ncrepo.netconomy.net/haproxy/haproxy-8u45-linux-x64.gz -o /opt/haproxy/haproxy-8u45-linux-x64.gz",
#    creates => "/opt/hybris/haproxy-8u45-linux-x64.gz",
#    notify  => Exec['unzip-fredhopper.zip'],
#  }

}
