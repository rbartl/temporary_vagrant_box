class nc_varnish {

  file { "/opt/hybris/varnish":
    ensure => directory,
  }

  file { "/opt/hybris/varnish.tgz": 
    source => "puppet:///modules/nc_varnish/varnish.tgz",
  }

  exec { "unzip-varnish" :
    command     => '/bin/tar -C /opt/hybris/varnish/ -xf /opt/mss/varnish.tgz',
    creates     => "/opt/hybris/varnish/bin/varnish",
  }

  # proxy: -x 10.41.5.10:3128
#  exec { "download-varnish.gz":
#    command => "/usr/bin/curl -s  http://ncrepo.netconomy.net/varnish/varnish-8u45-linux-x64.gz -o /opt/varnish/varnish-8u45-linux-x64.gz",
#    creates => "/opt/hybris/varnish-8u45-linux-x64.gz",
#    notify  => Exec['unzip-fredhopper.zip'],
#  }

}
