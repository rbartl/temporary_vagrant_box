

$prefix = '/opt/mms'

file { '/opt/mms' : 
  ensure => directory,
}

case $::hostname {
    /vlmmsad001/ : {
      include nc_nginx
      include nc_haproxy
      include nc_varnish
    }
    /vlmsad00[23]/ : {
      include nc_jdk
    }
}

#       include nc_elasticsearch
